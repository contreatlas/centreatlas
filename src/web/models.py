from pyexpat import model
from random import choices
from django.db import models

# Create your models here.


class Appointment(models.Model):

    DAYS = ('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi')
    TIMES = (   (None, '--:--'),
                ('09:00', '09:00'),
                ('10:00', '10:00'),
                ('11:00', '11:00'),
                ('12:00', '12:00'), 
                ('13:00', '13:00'),
                ('14:00', '14:00'),
                ('15:00', '15:00'),
                ('16:00', '16:00'), 
                ('17:00', '17:00'), 
                ('18:00', '18:00') )
    TIMES_MINUTES = (   (None, '--:--'),
                ('00 min', '00 min'),
                ('15 min', '15 min'),
                ('30 min', '30 min'),
                ('45 min', '45 min'), 
                )

    full_name = models.CharField(max_length=70, verbose_name='Nom complet')
    phone = models.CharField(max_length=20, verbose_name='Téléphoner')
    day = models.DateField(verbose_name="Date")
    time = models.CharField(max_length=20, verbose_name="temps", choices=TIMES)
    time_minute = models.CharField(max_length=20, verbose_name="temps par min", choices=TIMES_MINUTES)
    message = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, verbose_name="date création")

    class Meta:
        verbose_name = "Rendez-vous"