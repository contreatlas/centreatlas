from sqlite3 import Date
from django.forms import ModelForm
from .models import Appointment
from django import forms
from datetime import datetime
from django.core.exceptions import ValidationError
import datetime as dt
import calendar

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'


class AppointmentForm(ModelForm):

    # day = forms.DateField(widget=DateInput)
    # time = forms.TimeField(widget=TimeInput)
    # day = forms.DateField(required=True, input_formats=["%Y-%m-%d", ])
    # time = forms.TimeField(required=True, input_formats=["%H:%M", ])

    def __init__(self, *args, **kwargs):
        super(AppointmentForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control '})
            #
        date_now = datetime.now().date()
        date_now = date_now - dt.timedelta(days=1)
        last_day = date_now.replace(day = calendar.monthrange(date_now.year, date_now.month)[1])
        self.fields['full_name'].widget.attrs['placeholder'] = 'Nom complet'
        self.fields['phone'].widget.attrs['placeholder'] = 'Téléphone'
        self.fields['day'].widget.attrs['placeholder'] = '----/--/--'
        # self.fields['day'].widget.attrs['type'] = 'text'
        self.fields['time'].widget.attrs['placeholder'] = 'temps'
        self.fields['time_minute'].widget.attrs['placeholder'] = 'temps par min'
        self.fields['message'].widget.attrs['placeholder'] = 'message'

    class Meta:
        model = Appointment
        exclude = '__all__'
    



class ContactForm(forms.Form):

	name = forms.CharField(max_length = 50)
	subject = forms.CharField(max_length = 150)
	phone = forms.CharField(max_length = 50)
	email = forms.EmailField(max_length = 150)
	message = forms.CharField(widget = forms.Textarea, max_length = 2000)



    