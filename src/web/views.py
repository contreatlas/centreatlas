from threading import local
from django.shortcuts import render, redirect, reverse
from django.urls import reverse
from django.contrib import messages
from .functions import *
from web.forms import * 
from django.http import JsonResponse
# Create your views here.

def home(request):
    now_date = datetime.now().date().year
    disabled_days = get_disabled_days()
    form = AppointmentForm()
    if request.method == "POST":
        print("hello ")
        datetime_str = request.POST.get("day") + " " +request.POST.get("time")
        datetime_object = datetime.strptime(datetime_str, '%Y-%m-%d %H:%M')
        data = {
            "full_name" : request.POST.get("full_name"),
            "phone" : request.POST.get("phone"),
            "day" : request.POST.get("day"),
            "time" : request.POST.get("time"),
            "time_minute" : request.POST.get("time_minute"),
            "message" : request.POST.get("message"),
            }
        print(data," ..............................")
        form = AppointmentForm(data or None)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Votre rendez-vous a été ajouté avec succès')
        else:
            print(form.errors)
            messages.add_message(request, messages.ERROR, 'formulaire non valide')
            return render(request, 'pages/home.html', locals())
        return redirect(reverse('home_page'))

    return render(request, 'pages/home.html', locals())




def get_disabled_hours_view(request):
    disabled_hours = []
    if request.method == 'POST':


        date = request.POST.get("day")
        disabled_hours = get_disabled_hours(date)
        print(disabled_hours)
    return JsonResponse({"hours": disabled_hours})


def get_disabled_min_view(request):
    disabled_mins = []
    if request.method == 'POST':

        date = request.POST.get("day")
        time = request.POST.get("hour")
        print(time,"???????????????????????? ", date)
        disabled_mins = get_disabled_min(date, time)
        print(disabled_mins)
    return JsonResponse({"mins": disabled_mins})


from django.core.mail import send_mail




def send_mail_view(request):

    if request.method == 'POST':

        name = request.POST.get("name")
        email = request.POST.get("email")
        phone = request.POST.get("phone")
        subject = request.POST.get("subject")
        message = request.POST.get("message")
        subject_mail = f"vous avez un email de {name} / {email} / {phone} /  {subject}"
        send_mail(
            subject_mail,
            message,
            email,
            [ 'contreatlas@gmail.com', ],
            fail_silently=False,
        )
        print("Votre email a été envoyé avec succès")
        messages.add_message(request, messages.SUCCESS, 'Votre email a été envoyé avec succès')
        return redirect(reverse('home_page'))
    return redirect(reverse('home_page'))



def handle_not_found(request, exception):
    return render(request, 'pages/not_found.html',status=404)


def handle500(request):
    return render(request, 'pages/500.html',status=500)