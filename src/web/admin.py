from django.contrib import admin
from .models import Appointment
# Register your models here.


class AppointmentAdmin(admin.ModelAdmin):
    list_filter = ('full_name', 'phone', 'day', 'time')
    list_display = ("full_name", "phone", 'day', 'time', 'time_minute','message', 'created_at')
    search_fields = ('full_name', 'phone', 'day','time')

admin.site.register(Appointment, AppointmentAdmin)