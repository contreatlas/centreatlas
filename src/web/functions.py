from .models import *
import datetime

NUMBER_APOINTMENTS_PER_HOUR = 4
NUMBER_APOINTMENTS_PER_min = 1
NUMBER_HOURS_PER_DAY = 10
NUMBER_HOURS_PER_SUNDAY = 4
DATE_NOW = datetime.datetime.now().date()
TIMES = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00' ]
TIMES_MINUTES = ['00 min', '15 min', '30 min', '45 min' ]


def get_disabled_min(date, hour):
    print(type(date), " this is type of day")
    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    disabled_mins = []
    for time in TIMES_MINUTES:
        apointments = Appointment.objects.filter(day=date, time=hour, time_minute=time)
        print(apointments, "this is in function disabled minues", date)
        if apointments.count() >= NUMBER_APOINTMENTS_PER_min:
            disabled_mins.append(time)
    return disabled_mins


def get_disabled_hours(date):
    print(type(date), " this is type of day")
    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    disabled_hours = []
    for time in TIMES:
        apointments = Appointment.objects.filter(day=date, time=time)
        print(apointments, "this is in function disabled hours", date)
        if apointments.count() >= NUMBER_APOINTMENTS_PER_HOUR:
            disabled_hours.append(time)
    return disabled_hours


def get_disabled_days():
    apoints = Appointment.objects.filter(day__gte=DATE_NOW)
    # dates = list(set([a.day.strftime("%Y-%m-%d") for a in apoints]))
    dates = list(set([a.day for a in apoints]))
    disabled_days = []
    print(dates)
    for date in dates:
        if date.weekday() != 6:
            date_apoints = Appointment.objects.filter(day=date)
            date_apoints_numbers = date_apoints.count() 
            print(date)
            print(date_apoints_numbers, " ", date , "  ----------------- date_apoints_numbers ", NUMBER_APOINTMENTS_PER_HOUR * NUMBER_HOURS_PER_DAY)
            if date_apoints_numbers >= NUMBER_APOINTMENTS_PER_HOUR * NUMBER_HOURS_PER_DAY:
                disabled_days.append(date.strftime("%Y-%m-%d")) 
                print("add disabled days ")
        else:
            if date_apoints_numbers >= NUMBER_APOINTMENTS_PER_HOUR * NUMBER_HOURS_PER_SUNDAY:
                disabled_days.append(date.strftime("%Y-%m-%d")) 

    return disabled_days
    