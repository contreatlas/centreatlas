from unicodedata import name
from django.urls import path
from .views import *
urlpatterns = [
    path('', home, name="home_page"),
    path('disabled_hours', get_disabled_hours_view, name="disabled_hours"),
    path('disabled_mins', get_disabled_min_view, name="disabled_mins"),
    path('send_mail', send_mail_view, name="send_mail"),
    # path('appointment/create', create_Appointment, name="appointment_create"),
]

